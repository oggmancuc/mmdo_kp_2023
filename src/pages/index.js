import simplex from "@/pages/simplex";
import parse from 'html-react-parser';
import {Exo_2} from 'next/font/google'
import {useEffect, useState} from "react";

const exo = Exo_2({
    weight: '300', style: 'normal', display: 'swap', subsets: ['cyrillic-ext', 'latin-ext']
})

export default function Home() {
    const [rows, setRows] = useState(4);
    const [cols, setCols] = useState(4);
    const [tableData, setTableData] = useState([[19, 2, 3], [13, 2, 1], [15, 0, 3], [18, 3, 0]]);
    const [solution, setSolution] = useState('');
    const [objective, setObjective] = useState([7, 5]);
    const [objectiveFlag, setObjectiveFlag] = useState(0);
    const [constraints, setConstraints] = useState([[2, 3, 19, '<='], [2, 1, 13, '<='], [0, 3, 15, '<='], [3, 0, 18, '<=']]);

    useEffect(() => {
        let newConstrains = [];
        for (let i = 0; i < rows; i++) {
            let row = [];
            for (let j = 1; j < cols - 1; j++) {
                row.push(tableData[i][j]);
            }
            row.push(tableData[i][0])
            row.push('<=')
            newConstrains.push(row)
        }
        setConstraints(newConstrains)
    }, [tableData])

    useEffect(() => {
        setSolution(simplex(objective, constraints))
    }, [objectiveFlag, constraints])

    useEffect(() => {
        if (rows > tableData.length) {
            let newTableData = [...tableData];
            newTableData.push(Array(cols).fill(1));
            setTableData(newTableData);
        } else {
            let newTableData = [...tableData];
            newTableData = newTableData.slice(0, rows);
            setTableData(newTableData);
        }
    }, [rows])

    useEffect(() => {
        if (cols > tableData[0].length) {
            let newObjective = [...objective];
            let newObjectiveElements = Array((cols - 2) - objective.length).fill(1);
            newObjective.push(...newObjectiveElements);
            setObjective(newObjective);

            let newTableData = [...tableData];
            let newElements = Array(cols - 1 - tableData[0].length).fill(1);
            newTableData.forEach((row, index) => row.push(...newElements));
            setTableData(newTableData)
        } else {
            let newObjective = [...objective];
            newObjective = newObjective.slice(0, cols - 2);
            setObjective(newObjective);

            let newTableData = [...tableData];
            newTableData.forEach(row => row.slice(0, cols - 2));
            setTableData(newTableData);
            console.log('table', tableData)
        }
    }, [cols])

    const handleRowChange = (event) => {
        setRows(parseInt(event.target.value));
    };

    const handleColChange = (event) => {
        setCols(parseInt(event.target.value));
    };

    const handleDataChange = (event, rowIndex, colIndex) => {
        const newData = [...tableData];
        if (isNaN(parseInt(event.target.value))) {
            newData[rowIndex][colIndex] = 0;
        } else {
            newData[rowIndex][colIndex] = parseFloat(event.target.value);
        }
        setTableData(newData);
    };

    const handleObjectiveChange = (event, colIndex) => {
        const newData = [...objective];
        newData[colIndex] = parseFloat(event.target.value);
        setObjective(newData);
        if (objectiveFlag === 0) {
            setObjectiveFlag(1);
        } else {
            setObjectiveFlag(0)
        }
    };

    const renderTable = () => {
        let table = [];

        // Outer loop to create rows
        for (let i = 0; i < rows; i++) {
            let children = [];
            children.push(<td className="p-2" key={cols}>S<sub>{i + 1}</sub></td>);

            // Inner loop to create columns
            for (let j = 0; j < cols - 1; j++) {
                children.push(<td className="p-2" key={j}>
                    <input
                        className="bg-black text-center w-1/2 pl-3.5"
                        min="0"
                        type="number"
                        value={tableData[i] ? tableData[i][j] : 0}
                        onChange={(event) => handleDataChange(event, i, j)}
                    />
                </td>);
            }

            // Create the row with the columns
            table.push(<tr key={i}>{children}</tr>);
        }

        return table;
    };

    return (<main className={exo.className}>
        <div className="container mx-auto">
            <div className="text-center text-4xl mt-8">Обчислення задачі лінійного програмування</div>
            <div className="text-center text-xl mt-16">Для виготовлення
                продукції {cols - 2} видів {Array.from(Array(cols - 2).keys()).map(x => parse(`П<sub>${x + 1}</sub> `))} необхідно
                використовувати {rows} види
                сировини {Array.from(Array(rows).keys()).map(x => parse(`S<sub>${x + 1}</sub> `))}. Необхідно скласти
                такий план випуску
                продукції {Array.from(Array(cols - 2).keys()).map(x => parse(`П<sub>${x + 1}</sub> `))}, за якого
                прибуток підприємства від
                реалізації всієї продукції був би максимальним.
            </div>
            <div className="grid grid-cols-1 content-center lg:pl-64 lg:pr-64 mt-3 mb-16">
                <div className="flex flex-row justify-between mb-12 text-center">
                    <label>Кіл-сть видів продукції (+2)
                        <input className="input bg-black text-center" type="number" min="4" value={cols}
                               onChange={handleColChange}/>
                    </label>
                    <label>
                        Кіл-сть видів сировини
                        <input className="input bg-black text-center" type="number" min="2" value={rows}
                               onChange={handleRowChange}/>
                    </label>
                </div>
                <table className="table table-auto border-collapse text-center">
                    <tbody>
                    <tr>
                        <td className="p-2" rowSpan="2">Сировина</td>
                        <td className="p-2" rowSpan="2">Запаси сировини</td>
                        <td className="p-2" colSpan={objective.length}>Кіл-сть сировини для 1-ці
                            продукції
                        </td>
                    </tr>
                    <tr className="dark:border-neutral-500 border-b">
                        {Array.from(Array(cols - 2).keys()).map((index) => <td className="p-2"
                                                                               key={index}>П<sub>{index + 1}</sub>
                        </td>)}
                    </tr>
                    {renderTable()}
                    <tr className="border-t dark:border-neutral-500">
                        <td className="p-2" colSpan="2">Прибуток</td>
                        {objective.map((_, index) => <td className="p-2" key={index}>{<input
                            className="bg-black text-center w-1/2 pl-3.5"
                            min="0"
                            type="number"
                            value={objective[index]}
                            onChange={(event) => handleObjectiveChange(event, index)}
                        />

                        }</td>)}
                    </tr>
                    </tbody>
                </table>
            </div>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            <p className="text-center text-xl mt-16">Розв'язок:</p>
            {parse(solution)}
        </div>
        <footer className="border-t dark:border-neutral-500 h-6 text-center mt-16">
            Новічков Єгор ІПЗ-20-3<br/>2023
        </footer>
    </main>)
}

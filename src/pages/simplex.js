import * as math from 'mathjs'

export default function simplex(objectiveEquation, constraintEquations) {
    let string = '<div className="grid grid-cols-1 content-center lg:pl-64 lg:pr-64 mt-3">'
    string += `<p>F<sub>max</sub> = `
    let empty = true
    for (let i = 0; i < objectiveEquation.length; i++) {
        string += `${!empty && objectiveEquation[i] > 0 ? '+' + objectiveEquation[i] : objectiveEquation[i]}${'x' + '<sub>' + (i + 1) + '</sub>'}`
        empty = false
    }
    string += `<p className="mt-4">Вводимо додаткові змінні:</p>`
    for (let i = 0; i < constraintEquations.length; i++) {
        string += `<p>`
        empty = true
        for (let j = 0; j < constraintEquations[i].length - 2; j++) {
            if (constraintEquations[i][j] === 0) {
                continue
            }
            string += `${empty === false && constraintEquations[i][j] > 0 ? '+' + constraintEquations[i][j] : constraintEquations[i][j]}${'x' + '<sub>' + (j + 1) + '</sub>'}`
            empty = false
        }
        string += `${'+s' + '<sub>' + (i + 1) + '</sub>'}${constraintEquations[i][constraintEquations[i].length - 1] === '<=' ? '≤' : '≥'}${constraintEquations[i][constraintEquations[i].length - 2]}</p>`
    }
    string += `<p className="mt-4">Отримуємо початковий допустимий базисний розв'язок задачі:`
    for (let i = 0; i < constraintEquations.length && i < objectiveEquation.length; i++) {
        string += ` ${'x' + '<sub>' + (i + 1) + '</sub>'} = 0`
    }
    if (objectiveEquation.length > constraintEquations.length) {
        for (let i = constraintEquations.length; i < objectiveEquation.length; i++) {
            string += ` ${'x' + '<sub>' + (i + 1) + '</sub>'} = ${objectiveEquation[i]}`
        }
    }
    for (let i = 0; i < constraintEquations.length; i++) {
        string += ` s<sub>${i + 1}</sub> = ${constraintEquations[i][constraintEquations[i].length - 2]}`
    }

    string += `<p className="mt-4">Будуємо початкову симплекс-таблицю:</p>`
    // Initialize an empty table to store the simplex tableau
    let tableau = [];

    // Extract the coefficients of the objective function, excluding the 'Max' or 'Min' keyword
    const objectiveCoefficients = [...objectiveEquation];
    // console.log('objectiveCoefficients:', objectiveCoefficients);

    // Add zeros to the objectiveCoefficients array for each constraint equation
    for (let i = 0; i < constraintEquations.length; ++i) {
        objectiveCoefficients.push(math.fraction(0));
    }

    // Convert the constraint equations into the simplex tableau format
    constraintEquations.map((equation, index) => {
        let row = [0];

        row.push('s' + (index + 1));
        row.push(math.fraction(equation[equation.length - 2]));

        for (let i = 0; i < equation.length - 2; ++i) {
            row.push(math.fraction(equation[i]));
        }

        for (let i = 0; i < constraintEquations.length; ++i) {
            if (index === i) {
                row.push(math.fraction(1));
                continue;
            }
            row.push(math.fraction(0));
        }

        tableau.push(row);
    });


    // Set the maximum number of iterations to avoid infinite loops
    const maxIterations = 10;

    for (let iteration = 0; iteration < maxIterations; ++iteration) {
        // console.log(`\nTableau: ${iteration + 1}`);
        // console.table(tableau);
        string += getTableauString(tableau)

        // Calculate the zj values for the current tableau
        let zj = new Array(tableau[0].length - 2).fill(math.fraction(0));

        tableau.map((row, index) => {
            for (let j = 2; j < row.length; ++j) {
                zj[j - 2] = math.add(zj[j - 2], math.multiply(row[j], row[0]))
            }
        });

        // Calculate the zj-cj values for the current tableau
        let zjMinusCj = [];

        for (let j = 0; j < objectiveCoefficients.length; ++j) {
            // console.log('zj:', zj[j + 1], 'cj:', objectiveCoefficients[j]);
            zjMinusCj.push(math.subtract(zj[j + 1], objectiveCoefficients[j]));
        }

        // console.log('zj:', zj);
        // console.log('zj-cj:', zjMinusCj);
        string += getZjString(zj, zjMinusCj)

        // Determine the key column based on the zj-cj values
        let keyColumn = 3;
        let equationSolved = zjMinusCj[0] >= 0;

        zjMinusCj.map((num, index) => {
            if (num < zjMinusCj[keyColumn - 3] && num < 0) {
                keyColumn = index + 3;
                equationSolved = false;
            }
        });

        // If the equation is solved, break the loop
        if (equationSolved !== false) {
            let xes = []
            for (let i = 0; i < tableau.length; ++i) {
                if (tableau[i][1].includes('x')) {
                    xes.push([tableau[i][1], tableau[i][2]])
                }
            }

            string += '<p>В індексному рядку відсутні від\'ємні елементи, тому розв\'язок оптимальний.</p>'
            let fRounded = 0
            let fractionFlag = false
            for (let i = 0; i < xes.length; ++i) {
                if (!math.format(xes[i][1], {fraction: 'ratio'}).match(/(\/1\b)/)) {
                    fractionFlag = true
                }
                string += `<p>${xes[i][0]} = ${math.format(xes[i][1], {fraction: 'ratio'}).match(/(\/1\b)/) ? xes[i][1] : math.format(xes[i][1], {fraction: 'decimal'}) + ' або ' + math.format(xes[i][1], {fraction: 'ratio'}) + ' (округлюється до ' + math.floor(xes[i][1]) + ')'}</p>`
                fRounded += math.floor(xes[i][1]) * objectiveEquation[xes[i][0].slice(1) - 1]
            }

            string += `<p>F<sub>max</sub> = ${math.format(zj[0], {fraction: 'ratio'}).match(/(\/1\b)/) ? zj[0] : math.format(zj[0], {fraction: 'decimal'}) + ' або ' + math.format(zj[0], {fraction: 'ratio'})} ${fractionFlag ? ' (з округленими x: ' + fRounded + ')' : ''}</p>`
            string += '</div>'
            // console.log('Equation Solved');
            return string
        } else {
            string += `<p>В індексному рядку присутні від'ємні елементи, тому перераховуємо таблицю.</p><p>Знаходимо напрямний елемент:</p><p>Напрямний стовбчик: ${keyColumn + 1}</p>`
            // console.log('Key Column:', keyColumn + 1);

        }

        // Determine the key row based on the minimum ratio test
        let keyRow = 0;
        let prev = tableau[0][2] / tableau[0][keyColumn];

        if (prev <= 0) {
            prev = Number.MAX_SAFE_INTEGER;
        }

        tableau.map((row, index) => {
            const temp = row[2] / row[keyColumn];

            if (temp < prev && temp >= 0) {
                keyRow = index;
                prev = temp;
            }
        });

        string += `<p>Напрямний рядок: ${keyRow + 1}</p>`
        // console.log('Key Row:', keyRow + 1);

        // Simplify the tableau using the key row and key column
        simplifyTableau(keyRow, keyColumn, tableau, objectiveCoefficients);
    }

    // Define the function to simplify the tableau
    function simplifyTableau(keyRow, keyColumn, tableau, objectiveCoefficients) {
        const keyElement = tableau[keyRow][keyColumn];

        string += `<p>Напрямний елемент: ${math.format(math.fraction(keyElement), {fraction: 'ratio'}).match(/(\/1\b)/) ? math.evaluate(math.format(math.fraction(keyElement), {fraction: 'ratio'})) : math.format(math.fraction(keyElement), {fraction: 'ratio'})}</p><p className="mt-4 mb-0.5">Перерахована таблиця:</p>`
        // console.log('Key Element:', keyElement);

        // Divide the key row by the key element
        for (let i = 2; i < tableau[keyRow].length; i++) {
            tableau[keyRow][i] = math.divide(tableau[keyRow][i], keyElement);
        }

        // Update the other rows based on the key row
        tableau.map((row, i) => {
            if (i !== keyRow) {
                let constant = row[keyColumn];

                for (let j = 2; j < row.length; ++j) {
                    tableau[i][j] = math.subtract(tableau[i][j], math.multiply(constant, tableau[keyRow][j]));
                }
            }
        });

        // Update the basic variable in the key row
        tableau[keyRow][0] = objectiveCoefficients[keyColumn - 3];
        tableau[keyRow][1] = keyColumn - 2 <= objectiveCoefficients.length - constraintEquations.length ? `x${keyColumn - 2}` : `s${keyColumn - constraintEquations[0].length}`;
    }

    function getTableauString(tableau) {
        let tableauString = '<table className="table-fixed border-collapse text-center"><tbody>'
        for (let i = 0; i < tableau.length; ++i) {
            tableauString += '<tr>'
            for (let j = 0; j < tableau[i].length; j++) {
                tableauString += `<td className="p-1 border dark:border-neutral-500">${typeof tableau[i][j] === 'string' ? tableau[i][j] : math.format(math.fraction(tableau[i][j]), {fraction: 'ratio'}).match(/(\/1\b)/) ? math.evaluate(math.format(math.fraction(tableau[i][j]), {fraction: 'ratio'})) : math.format(math.fraction(tableau[i][j]), {fraction: 'ratio'})}</td>`
            }
            tableauString += '</tr>'
        }
        return tableauString
    }

    function getZjString(zj, zjMinusCj) {
        let zjString = `<tr><td colSpan="2"></td><td className="p-1 border dark:border-neutral-500">${math.format(math.fraction(zj[0]), {fraction: 'ratio'}).match(/(\/1\b)/) ? math.evaluate(math.format(math.fraction(zj[0]))) : math.format(math.fraction(zj[0]), {fraction: 'ratio'})}</td>`
        for (let i = 0; i < zjMinusCj.length; ++i) {
            zjString += `<td className="p-1 border dark:border-neutral-500">${math.format(math.fraction(zjMinusCj[i]), {fraction: 'ratio'}).match(/(\/1\b)/) ? math.evaluate(math.format(math.fraction(zjMinusCj[i]), {fraction: 'ratio'})) : math.format(math.fraction(zjMinusCj[i]), {fraction: 'ratio'})}</td>`
        }
        zjString += '</tr></tbody></table>'
        return zjString
    }
}



